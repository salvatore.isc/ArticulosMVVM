//
//  ViewController.swift
//  ArticulosMVVM
//
//  Created by Salvador Lopez on 29/06/23.
//

import UIKit

class ViewController: UIViewController {
    
    var articleListVM: ArticleListViewModel!

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tableView.rowHeight = 160
        tableView.delegate = self
        tableView.dataSource = self
        articleListVM = ArticleListViewModel()
        articleListVM.loadArticles { articles in
            if let articles = articles {
                self.articleListVM?.articles = articles
            }
            // ReloadTableView
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }


}

extension ViewController: UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return articleListVM == nil ? 0 : articleListVM.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        articleListVM.numberOfRowsInSection(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let articleVM = self.articleListVM.articleAtIndex(indexPath.row)
        cell.textLabel?.text = articleVM.titulo
        cell.detailTextLabel?.text = articleVM.description
        return cell
    }
}
