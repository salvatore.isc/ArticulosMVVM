//
//  Practica.swift
//  ArticulosMVVM
//
//  Created by Salvador Lopez on 30/06/23.
//

import Foundation

//MARK: PRACTICA 5/6

/**
 Practica: 5/6
 Objetivo: Reforzar el conocimiento adquirido en el capitulo 6 (Grand Central Dispatch) y capitulo 7 (Arquitectura de aplicaciones).
 
 Requerimientos:
 
 Partiendo de la practica numero 2 y 4 , en donde se muestra una lista de medicamentos.
 Realice el consumo de imagenes en esta URL[https://jsonplaceholder.typicode.com/photos], y adapte el codigo para mostrar en la lista de medicamnetos una imagen (thumbnailUrl) dentro de la celda de cada medicamento.
 Posteriormente, guarde una version cuando haya realizado la carga de imagenes con el uso de GCD en la lista de medicamentos.
 Para la segunda parte de esta practica, realice la creacion de un clase o estructura para adaptar su proyecto a una arquitectura MVVM.
 Adicional, validar que no se pierda el funcionamiento relacionado a la practica numero 2 y numero 4.
 
 [salvatore.isc@gmail.com]
 */
