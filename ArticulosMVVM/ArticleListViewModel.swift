//
//  ArticleListViewModel.swift
//  ArticulosMVVM
//
//  Created by Salvador Lopez on 29/06/23.
//

import Foundation

//MARK: ARTICLE_LIST

// Estructura para la lista de articulos
struct ArticleListViewModel{
    var articles = [Articulo]()
}

// Extension para cargar la lista de articulos
extension ArticleListViewModel{
    func loadArticles(completion: @escaping ([Articulo]?) -> Void){
        let url = URL(string: Constantes.restApiUrl)!
        WebService().getArticles(url: url) { articles in
            if let articles = articles {
                completion(articles)
            }else{
                completion(nil)
            }
        }
    }
}

// Extension para funciones para pasar datos que requere el VC (TableView).
extension ArticleListViewModel {
    
    var numberOfSections: Int {
        return 1
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        return self.articles.count
    }
    
    // Conocer y retornar un articulo en base a su index
    func articleAtIndex(_ index: Int) -> ArticleViewModel{
        let article = self.articles[index]
        return ArticleViewModel(article)
    }
    
}

//MARK: ARTICLE

// Para el manejo de Articlo individual
struct ArticleViewModel{
    private let article: Articulo
}

// Extension - Metodo init para cada Articilo
extension ArticleViewModel{
    init(_ article: Articulo){
        self.article = article
    }
}

// Obtener las propiedades de cada Articulo
extension ArticleViewModel {
    var titulo: String{
        return self.article.title!
    }
    var description: String{
        return self.article.description ?? ""
    }
}
